# Git
## Menú
- [Que es git?](#Que-es-git)
- [Comandos de git en consola](#Comandos-de-git-en-consola)
- [Clientes git](#Clientes-git)
- [Clonación de proyecto por consola y por cliente](#Clonación-de-proyecto-por-consola-y-por-cliente)
- [Commits por consola y por cliente kraken o smart](#Commits-por-consola-y-por-cliente-kraken-o-smart)
- [Ramas de kraken](#Ramas-de-kraken)
- [Merge](#Merge)

## Que es git?
Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, Git es un proyecto de código abierto maduro y con un mantenimiento activo que desarrolló originalmente Linus Torvalds, el famoso creador del kernel del sistema operativo Linux, en 2005. Un asombroso número de proyectos de software dependen de Git para el control de versiones, incluidos proyectos comerciales y de código abierto. Los desarrolladores que han trabajado con Git cuentan con una buena representación en la base de talentos disponibles para el desarrollo de software, y este sistema funciona a la perfección en una amplia variedad de sistemas operativos e IDE (entornos de desarrollo integrados).
## Comandos de Git en consola
Los comandos básicos que se utilizan en git son:

>git help

    Muestra una lista con los comandos más utilizados en GIT.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20help.png)

>git init

    Podemos ejecutar ese comando para crear localmente un repositorio con GIT y así utilizar todo el funcionamiento que GIT ofrece.  Basta con estar ubicados dentro de la carpeta donde tenemos nuestro proyecto y ejecutar el comando.  Cuando agreguemos archivos y un commit, se va a crear el branch master por defecto.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20init.png)

>git commit -m "mensaje" + archivos

    Hace commit a los archivos que indiquemos, de esta manera quedan guardados nuestras modificaciones.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20commit.png)

>git commit -am "mensaje"

    Hace commit de los archivos que han sido modificados y GIT los está siguiendo.

>git checkout -b NombreDeBranch

    Crea un nuevo branch y automaticamente GIT se cambia al branch creado, clonando el branch desde donde ejecutamos el comando.

>git branch

    Nos muestra una lista de los branches que existen en nuestro repositorio.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20branch.png)

>git checkout NombreDeBranch

    Sirve para moverse entre branches, en este caso vamos al branch que indicamos en el comando.

>git merge NombreDeBranch

    Hace un merge entre dos branches, en este caso la dirección del merge sería entre el branch que indiquemos en el comando, y el branch donde estémos ubicados.

>git status

    Nos indica el estado del repositorio, por ejemplo cuales están modificados, cuales no están siendo seguidos por GIT, entre otras características.

>git clone URL/name.git NombreProyecto

    Clona un proyecto de git en la carpeta NombreProyecto.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20clone.png)

>git push origin NombreDeBranch

    Luego de que hicimos un git commit, si estamos trabajando remotamente, este comando va a subir los archivos al repositorio remoto.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20push.png)

>git pull origin NombreDeBranch

    Hace una actualización en nuestro branch local, desde un branch remoto que indicamos en el comando.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20pull.png)

# Clientes Git
Los clientes de la interfaz gráfica de usuario (GUI, por sus siglas en inglés) son herramientas que proporcionan una visualización alternativa para Git.

## Clonación de proyecto por consola y por cliente
Para clonar por consola usarmos el comando mencionado anteriormente que es "git clone URL/name.git NombreProyecto" con esto se crearia automaticamente el clon del archivo.


![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20clone.png)

Y para clonar por cliente en este caso usaremos Kraken primero habrimos la consola de archivos y ponemos en clonar, una ves eso yo en este caso usare un archivo que tengo en GitLab entonces celeciono el archivo a clonar en la parte de arriba dice donde lo vamos a guardar, en la mitad el archivo y en la parte de abajo el nombre con el que se clona.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/kraken.png)

## Commit por consola y por cliente
Para crear un commit por consola usamos los comandos en este caso sera el comando "git commit -m "mensaje" + archivos"

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/git%20commit.png)

Y para crear commit por cliente usamos otra ves Kraken, selecionamos los cambios, y le agregamos una descripcion y asi generariamos el commit.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/commit.png)

## Ramas de kraken
Una rama Git es simplemente un apuntador móvil apuntando a una de esas confirmaciones. La rama por defecto de Git es la rama master . Con la primera confirmación de cambios que realicemos, se creará esta rama principal master apuntando a dicha confirmación.

Para crear una rama en Kraken pulsamos en "Branch", selecionamos un nombre para la nueva rama y le ponemos enter.

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/rama.png)

## Merge
La fusión es la forma que tiene Git de volver a unir un historial bifurcado. El comando git merge permite tomar las líneas independientes de desarrollo creadas por git branch e integrarlas en una sola rama.

Asi se uniria la rama en Kraken, la rama creada con la rama "Master"

![img](https://gitlab.com/bas.chipantasi/capacitaciones-yv-mlab-2021/-/raw/master/imagenes/merge.png)