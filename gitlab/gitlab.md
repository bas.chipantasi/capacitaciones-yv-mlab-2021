# GitLab
## Menú
- [Información de como acceder](#Información-de-como-acceder)
- [Crear repositorios](#Crear-repositorios)
- [Crear grupos](#Crear-grupos)
- [Crear subgrupos](#Crear-subgrupos)
- [Crear issues](#Crear-issues)
- [Crear labels](#Crear-labels)
- [Roles que cumplen](#Roles-que-cumplen)
- [Agregar miembros](#Agregar-miembros)
- [Crear borads y manejo de boards](#Crear-borads-y-manejo-de-boards)

## Informacion de como acceder
Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.


## Crear repositorio
Un repositorio es un espacio centralizado donde se almacena, organiza, mantiene y difunde información digital, habitualmente archivos informáticos, que pueden contener trabajos científicos, conjuntos de datos o software.

Para crear uno en GitLab se crear primero un nuevo proyecto, una vez dentro de este en la parte izquierda entre las herramientas ya tendriamos creado el repositorio
## Crear Grupos

Para crear un grupo:

Desde el menú superior, ya sea:
Seleccione Grupos> Sus grupos y, a la derecha, seleccione el botón Nuevo grupo .
A la izquierda del cuadro de búsqueda, seleccione el signo más y luego Nuevo grupo .
Para el nombre del grupo , use solo:
Caracteres alfanuméricos
Emojis
Subrayados
Guiones, puntos, espacios y paréntesis (sin embargo, no puede comenzar con ninguno de estos caracteres)
Para obtener una lista de palabras que no se pueden usar como nombres de grupos, consulte Nombres reservados .

Para la URL del grupo , que se usa para el espacio de nombres , use solo:
Caracteres alfanuméricos
Subrayados
Guiones y puntos (no puede comenzar con guiones ni terminar con un punto)
Elija el nivel de visibilidad .
Invite a miembros de GitLab u otros usuarios a unirse al grupo.

## Crear subgrupos

Para crear un subgrupo:

1 - En el panel del grupo, haga clic en el botón Nuevo subgrupo .



2 - Crea un nuevo grupo como lo harías normalmente. Observe que el espacio de nombres del grupo principal inmediato se fija en Ruta del grupo . El nivel de visibilidad puede diferir del grupo principal inmediato.



3 - Haga clic en el botón Crear grupo para ser redirigido a la página del panel del nuevo grupo.

Siga el mismo proceso para crear grupos posteriores.

## Crear issues

Un Issue es una nota en un repositorio que trata de llamar la atención sobre un problema. En GitHub puedes etiquetar, buscar o asignar Issues, haciendo que la gestión de un proyecto activo sea más sencilla.

## Crear labels

Los propietarios de la organización pueden administrar las etiquetas predeterminadas para los repositorios de la organización.

Las etiquetas predeterminadas se incluirán en todos los repositorios nuevos de tu organización, pero luego cualquier usuario con acceso de escritura al repositorio puede editar o eliminar las etiquetas de ese repositorio. Agregar, editar o eliminar una etiqueta predeterminada no agrega, edita o elimina la etiqueta de los repositorios existentes.

El procedimiento es:

1- Acceder al issues

2- Acceder a issues -> Labels

3- Se le otorga un nombre al Label y si gusta un color y ya tendria su label

## Roles que cumplen
Los miembros de la comunidad de GitLab tienen sus privilegios y responsabilidades las cuales veremos a continuacion.

>Mantenedor	
    
    Acepta solicitudes de fusión en varios proyectos de GitLab este usuario esta agregado a la página del equipo . Un experto en revisiones de código y conoce el producto / código base

>Crítico	
    
    Realiza revisiones de código en MR este usuario esta gregado a la página del equipo

>Desarrollador	

    Tiene acceso a la infraestructura y problemas internos de GitLab (por ejemplo, relacionados con recursos humanos) Es un empleado de GitLab o miembro del equipo central (con un NDA)

>Contribuyente:
    
    Puede hacer contribuciones a todos los proyectos públicos de GitLab	siempre y cuando tenga una cuenta de GitLab.com

## Agregar miembros

Paso 1 : inicie sesion en su cuenta de GitLab y navegue hasta su proyecto en Proyectos .


Paso 2 - Luego haga clic en la opcion Miembros debajo de la pestana Configuracion -

Paso 3 - Esto abrira la pantalla de abajo para agregar el miembro a su proyecto -

Paso 4 - Ahora ingrese el nombre d 'usuario, permiso de funcion, fecha de vencimiento (opcional) y haga clic en el boton Agregar al proyecto para agregar un usuario al proyecto -

Paso 5 : luego recibira un mensaje exitoso despues de agregar el usuario al proyecto.

El cuadro resaltado en la imagen de arriba indica que se ha agregado un nuevo usuario al proyecto -

Paso 6 - Tambien puede agregar un usuario al proyecto haciendo clic en el boton Importar -

Paso 7 - Ahora seleccione el proyecto desde el cual desea agregar el usuario para su proyecto y haga clic en el boton Importar miembros del proyecto -

Paso 8 - Recibira su mensaje de exito despues de importar el usuario al proyecto

## Crear borads y manejo de boards
Para crear un nuevo tablero de problemas:

1 - Haga clic en el menú desplegable con el nombre del tablero actual en la esquina superior izquierda de la página Tableros de temas.

2 - Haz clic en Crear tablero nuevo .

3 - Ingrese el nombre de la nueva placa y seleccione su alcance: hito, etiquetas, cesionario o peso